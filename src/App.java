import java.util.ArrayList;
import java.util.Date;

import com.devcamp.j03_javabasic.s50.Order2;
import com.devcamp.j03_javabasic.s50.Person;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order2> arrayList = new ArrayList<>();
        //Khởi tạo các object order
        Order2 order11 = new Order2();
        Order2 order22 = new Order2(2, "Nguyen Van B", 125000);
        Order2 order33 = new Order2(3, "Nguyen Van C", 150000, new Date(), true);
        Order2 order44 = new Order2(4, "Nguyen Van D", 200000, new Date(), false, new String[]{"pizza", "coca", "cheese"}, new Person("Nguyen Van E"));
        //Thêm các object order vào araylist
        arrayList.add(order11);
        arrayList.add(order22);
        arrayList.add(order33);
        arrayList.add(order44);
        //In ra màn hình
        for(Order2 order : arrayList){
            System.out.println(order.toString());
        }
    }
}
