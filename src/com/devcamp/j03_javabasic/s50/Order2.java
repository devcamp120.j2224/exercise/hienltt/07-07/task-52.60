package com.devcamp.j03_javabasic.s50;

import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class Order2 {
    int id; // id của order
    String customerName; // tên khách hàng
    long price;// tổng giá tiền
    Date orderDate; // ngày thực hiện order
    Boolean confirm; // đã xác nhận hay chưa?
    String[] items; // danh sách mặt hàng đã mua
    Person buyer;// người mua, là một object thuộc class Person

    // Khởi tạo với 0 tham số
    public Order2(){
        this.buyer = new Person();
        this.orderDate = new Date();
        this.items = new String[] {"Chips", "Pepsi","Phomat"};
    }
    //Khởi tạo với 3 tham số
    public Order2(int id, String customerName, Integer price){
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = new Date();
        this.buyer = new Person("Nguyen Van F");
        this.items = new String[] {"Lavie", "Hambuger", "KFC"};
    }
    //Khởi tạo với 5 tham số
    public Order2(int id, String customerName, Integer price, Date orderDate, Boolean confirm){
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.buyer = new Person("Nguyen Van G");
        this.items = new String[] {"Fanta", "Pizza", "Chips"};
    }
    //Khởi tạo với tất cả tham số
    public Order2(int id, String customerName, Integer price, Date orderDate, boolean confirm, String[] items, Person buyer){
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items = items;
        this.buyer = buyer;
    }
    @Override
    public String toString(){
        // Đinh dạng tiêu chuẩn VN
        Locale.setDefault(new Locale("vi", "VN"));
        // Định dạng cho ngày tháng
        String pattern = "dd-MMMM-yyy HH:mm:ss.SSS";
        DateTimeFormatter defaulTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        // Định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        //Trả về chuỗi
        String Return = "[";
        Return += "id: " + this.id + ", ";
        Return += "customerName: " + this.customerName + ", ";
        Return += "price: " + usNumberFormat.format(this.price) + ", ";
        Return += "orderDate: " + defaulTimeFormatter.format(this.orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()) + ", ";
        Return += "confirm: " + this.confirm  + ", ";
        Return += "items:[{ ";
        if (this.items != null) {
            for(String item : items) {
                Return += item + ", ";
            }
        }else{
            Return += "null, ";
        }
        Return += "]}, ";
        Return += "person: " + this.buyer.name;
        return Return;

        // return "Order [id=" + id
        //     + ", customerName=" + customerName
        //     + ", price=" + usNumberFormat.format(price)
        //     + ", orderDate=" + defaulTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
        //     + ", confirm=" + confirm
        //     + ", items=[" + items[0] + ", " + items[1] + ", " + items[2] + "]"
        //     + ",buyer=" + buyer.name + "]";
    }
}
